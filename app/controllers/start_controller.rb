class StartController < ApplicationController
  def new
  end

  def start
    begin
      @universe = Universe.new(params[:input])
      if @universe.errors.empty?
        @universe.run
      else
        redirect_to start_new_path, :alert => "#{@universe.errors.full_messages.to_sentence}"
      end
    rescue TypeError => e
      redirect_to start_new_path, :alert => "#{e}"
    end
  end
end
