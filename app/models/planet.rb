class Planet < ActiveRecord::Base
  attr_accessor :name
  attr_accessor :x_boundary
  attr_accessor :y_boundary

  def initialize(name, x_boundary = 1, y_boundary = 1)
    self.errors.add(:base, "X boundary is over 50 for this planet") if x_boundary > 50
    self.errors.add(:base, "Y boundary is over 50 for this planet") if y_boundary > 50
    @name = name
    @x_boundary = x_boundary
    @y_boundary = y_boundary
  end
end
