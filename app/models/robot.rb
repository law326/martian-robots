class Robot < ActiveRecord::Base
	attr_accessor :orientation
	attr_accessor :x_position
	attr_accessor :y_position
	attr_accessor :status

	def initialize(planet, x_position = 0, y_position = 0, orientation = 'N')
		@deployed_planet = planet.name
		@x_boundary = planet.x_boundary
		@y_boundary = planet.y_boundary
		@x_position = x_position
		@y_position = y_position
		@orientation = orientation.upcase
		@status = nil
	end

	def start_robot(instruction)
		instruction.each_char do |action|
			case action.upcase
			when 'R'
				self.turn_right
			when 'L'
				self.turn_left
			when 'F'
				next if self.ignore_forward_command?
				self.move_forward
				if self.is_off_the_boundary?
					@status = 'LOST'
					last_coordinate = self.get_last_step
					self.leave_scent(last_coordinate[:x_position], last_coordinate[:y_position])
				end
			else
				raise TypeError.new('Robot does not understand such command')
			end
		end
	end

	def turn_left
		case @orientation
		when 'N'
			@orientation = 'W'
		when 'S'
			@orientation = 'E'
		when 'E'
			@orientation = 'N'
		when 'W'
			@orientation = 'S'
		else
			raise TypeError.new('Robot cannot turn left such orientation')
		end
	end

	def turn_right
		case @orientation
		when 'N'
			@orientation = 'E'
		when 'S'
			@orientation = 'W'
		when 'E'
			@orientation = 'S'
		when 'W'
			@orientation = 'N'
		else
			raise TypeError.new('Robot cannot turn right with such orientation')
		end
	end

	def move_forward
		case @orientation
		when 'N'
			@y_position += 1
		when 'S'
			@y_position -= 1
		when 'E'
			@x_position += 1
		when 'W'
			@x_position -= 1
		else
			raise TypeError.new('Robot cannot move forward with such orientation')
		end
	end

	def ignore_forward_command?
		return false if !self.is_marked?(@x_position, @y_position)
		case @orientation
		when 'N'
			return true if self.is_off_the_boundary?(@x_position, @y_position + 1)
		when 'S'
			return true if self.is_off_the_boundary?(@x_position, @y_position - 1)
		when 'E'
			return true if self.is_off_the_boundary?(@x_position + 1, @y_position)
		when 'W'
			return true if self.is_off_the_boundary?(@x_position - 1, @y_position)
		else
			raise TypeError.new('No such orientation')
		end
		return false
	end

	def get_last_step
		current_coordinate = Hash.new
		current_coordinate[:x_position] = @x_position
		current_coordinate[:y_position] = @y_position
		case @orientation
		when 'N'
			current_coordinate[:y_position] -= 1
		when 'S'
			current_coordinate[:y_position] += 1
		when 'E'
			current_coordinate[:x_position] -= 1
		when 'W'
			current_coordinate[:x_position] += 1
		else
			raise TypeError.new('No such orientation')
		end
		return current_coordinate
	end

	def is_off_the_boundary?(x = nil, y = nil)
		x = @x_position if x.nil?
		y = @y_position if y.nil?
		if x > @x_boundary || x < 0 || y > @y_boundary || y < 0
			return true
		else
			return false
		end
	end

	def leave_scent(x = nil, y = nil)
		x = @x_position if x.nil?
		y = @y_position if y.nil?
		mark = RobotMark.new
		mark.planet = @deployed_planet
		mark.x_coordinate = x.to_i
		mark.y_coordinate = y.to_i
		mark.save
	end

	def is_marked?(x = nil, y = nil)
		x = @x_position if x.nil?
		y = @y_position if y.nil?
		mark = RobotMark.find_by_x_coordinate_and_y_coordinate(x, y)
		if mark
			return true
		else
			return false
		end
	end

end
