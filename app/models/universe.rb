class Universe < ActiveRecord::Base
	attr_accessor :planet
	attr_accessor :robots

	def initialize(instruction)
		self.errors.add(:base, "Your mission instruction is blank!") if instruction.blank?
		return self if !self.errors.empty?
		RobotMark.destroy_all
		planet_boundary = instruction.lines.first.split
		@planet = Planet.new('Mars', planet_boundary[0].to_i, planet_boundary[1].to_i)
		self.errors.add(:base, "#{@planet.errors.full_messages.to_sentence}") if !@planet.errors.empty?
		return self if !self.errors.empty?
		@robots = Array.new
		line_count = 0
		robot_count = 0
		robot_hash = Hash.new
		instruction.each_line do |line|
			next if line == instruction.lines.first
			next if line.blank?
			line_count += 1
			case line_count
			when 1
				robot_settings = line.split
				robot_hash[:robot] = Robot.new(@planet, robot_settings[0].to_i, robot_settings[1].to_i, robot_settings[2])
			when 2
				robot_hash[:command] = line.gsub(/\r\n?/, "")
				robots[robot_count] = robot_hash
				robot_hash = Hash.new
				line_count = 0
				robot_count += 1
			end
		end
	end

	def run
		@robots.each do |robot|
			robot[:robot].start_robot(robot[:command])
		end
	end
end
