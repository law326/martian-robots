class CreateRobotMarks < ActiveRecord::Migration
  def change
    create_table :robot_marks do |t|
      t.integer :x_coordinate
      t.integer :y_coordinate

      t.timestamps
    end
  end
end
