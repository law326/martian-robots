require 'test_helper'

class StartControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end

  test "should post start" do
    post :start, post: {input: "5  3\r\n1  1  E\r\nRFRFRFRF\r\n3  2  N\r\nFRRFLLFFRRFLL\r\n0  3  W\r\nLLFFFLFLFL\r\n"}
    assert_response :found
  end

end
