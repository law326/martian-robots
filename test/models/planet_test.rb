require 'test_helper'

class PlanetTest < ActiveSupport::TestCase
  def test_create_a_planet
  	mars = Planet.new('Mars', 5, 5)
  	expected_planet_name = 'Mars'
  	expected_x_boundary = 5
  	expected_y_boundary = 5
  	assert_equal(expected_planet_name, mars.name)
  	assert_equal(expected_x_boundary, mars.x_boundary)
  	assert_equal(expected_y_boundary, mars.y_boundary)
  end
end
