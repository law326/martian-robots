require 'test_helper'

class RobotTest < ActiveSupport::TestCase
  def test_start_robot
    mars = Planet.new('Mars', 3, 3)
    robot_1 = Robot.new(mars, 1, 1, 'E')
    instruction = 'RFRFRFRF'
    robot_1.start_robot(instruction)
    expected_x_coordinate = 1
    expected_y_coordinate = 1
    expected_orientation = 'E'
    assert_equal(expected_x_coordinate, robot_1.x_position)
    assert_equal(expected_y_coordinate, robot_1.y_position)
    assert_equal(expected_orientation, robot_1.orientation)

    robot_2 = Robot.new(mars, 0, 3, 'W')
    instruction = 'LLFFFLFLFL'
    robot_2.start_robot(instruction)
    expected_x_coordinate = 2
    expected_y_coordinate = 4
    expected_orientation = 'S'
    expected_status = 'LOST'
    assert_equal(expected_x_coordinate, robot_2.x_position)
    assert_equal(expected_y_coordinate, robot_2.y_position)
    assert_equal(expected_orientation, robot_2.orientation)
    assert_equal(expected_status, robot_2.status)

    robot_3 = Robot.new(mars, 0, 3, 'W')
    instruction = 'LLFFFFLFLFF'
    robot_3.start_robot(instruction)
    expected_x_coordinate = 1
    expected_y_coordinate = 3
    expected_orientation = 'W'
    expected_status = nil
    assert_equal(expected_x_coordinate, robot_3.x_position)
    assert_equal(expected_y_coordinate, robot_3.y_position)
    assert_equal(expected_orientation, robot_3.orientation)
    assert_equal(expected_status, robot_3.status)
  end

  def test_turn_left
    mars = Planet.new('Mars', 3, 3)
    robot_1 = Robot.new(mars, 0, 0, 'N')
    robot_1.turn_left
    expected_x_coordinate = 0
    expected_y_coordinate = 0
    expected_orientation = 'W'
    assert_equal(expected_x_coordinate, robot_1.x_position)
    assert_equal(expected_y_coordinate, robot_1.y_position)
    assert_equal(expected_orientation, robot_1.orientation)

    robot_2 = Robot.new(mars, 1, 1, 'W')
    robot_2.turn_left
    expected_x_coordinate = 1
    expected_y_coordinate = 1
    expected_orientation = 'S'
    assert_equal(expected_x_coordinate, robot_2.x_position)
    assert_equal(expected_y_coordinate, robot_2.y_position)
    assert_equal(expected_orientation, robot_2.orientation)
  end

  def test_turn_right
    mars = Planet.new('Mars', 3, 3)
    robot_1 = Robot.new(mars, 0, 0, 'N')
    robot_1.turn_right
    expected_x_coordinate = 0
    expected_y_coordinate = 0
    expected_orientation = 'E'
    assert_equal(expected_x_coordinate, robot_1.x_position)
    assert_equal(expected_y_coordinate, robot_1.y_position)
    assert_equal(expected_orientation, robot_1.orientation)

    robot_2 = Robot.new(mars, 1, 1, 'W')
    robot_2.turn_right
    expected_x_coordinate = 1
    expected_y_coordinate = 1
    expected_orientation = 'N'
    assert_equal(expected_x_coordinate, robot_2.x_position)
    assert_equal(expected_y_coordinate, robot_2.y_position)
    assert_equal(expected_orientation, robot_2.orientation)
  end

  def test_move_forward
    mars = Planet.new('Mars', 3, 3)
    robot_1 = Robot.new(mars, 0, 0, 'N')
    robot_1.move_forward
    expected_x_coordinate = 0
    expected_y_coordinate = 1
    expected_orientation = 'N'
    assert_equal(expected_x_coordinate, robot_1.x_position)
    assert_equal(expected_y_coordinate, robot_1.y_position)
    assert_equal(expected_orientation, robot_1.orientation)

    robot_2 = Robot.new(mars, 1, 1, 'E')
    robot_2.move_forward
    expected_x_coordinate = 2
    expected_y_coordinate = 1
    expected_orientation = 'E'
    assert_equal(expected_x_coordinate, robot_2.x_position)
    assert_equal(expected_y_coordinate, robot_2.y_position)
    assert_equal(expected_orientation, robot_2.orientation)
  end

  def test_ignore_forward_command
    RobotMark.create(:planet => 'Mars', :x_coordinate => 0, :y_coordinate => 2)
    mars = Planet.new('Mars', 2, 2)
    robot_1 = Robot.new(mars, 0, 2, 'N')
    expected_x_coordinate = 0
    expected_y_coordinate = 2
    expected_orientation = 'N'
    expected_return = true
    assert_equal(expected_return, robot_1.ignore_forward_command?)
    assert_equal(expected_x_coordinate, robot_1.x_position)
    assert_equal(expected_y_coordinate, robot_1.y_position)
    assert_equal(expected_orientation, robot_1.orientation)

    robot_2 = Robot.new(mars, 0, 2, 'N')
    robot_2.turn_right
    expected_x_coordinate = 0
    expected_y_coordinate = 2
    expected_orientation = 'E'
    expected_return = false
    assert_equal(expected_return, robot_2.ignore_forward_command?)
    assert_equal(expected_x_coordinate, robot_2.x_position)
    assert_equal(expected_y_coordinate, robot_2.y_position)
    assert_equal(expected_orientation, robot_2.orientation)
  end

  def test_get_last_step
    mars = Planet.new('Mars', 3, 3)
    robot_1 = Robot.new(mars, 0, 0, 'N')
    robot_1.move_forward
    last_setp = robot_1.get_last_step
    expected_x_coordinate = 0
    expected_y_coordinate = 0
    expected_orientation = 'N'
    assert_equal(expected_x_coordinate, last_setp[:x_position])
    assert_equal(expected_y_coordinate, last_setp[:y_position])
    assert_equal(expected_orientation, robot_1.orientation)

    robot_2 = Robot.new(mars, 1, 1, 'E')
    robot_2.move_forward
    last_setp = robot_2.get_last_step
    expected_x_coordinate = 1
    expected_y_coordinate = 1
    expected_orientation = 'E'
    assert_equal(expected_x_coordinate, last_setp[:x_position])
    assert_equal(expected_y_coordinate, last_setp[:y_position])
    assert_equal(expected_orientation, robot_2.orientation)
  end

  def test_is_off_the_boundary
    mars = Planet.new('Mars', 3, 3)
    robot_1 = Robot.new(mars, 0, 0, 'N')
    robot_1.move_forward
    robot_1.move_forward
    robot_1.move_forward
    robot_1.move_forward
    expected = true
    assert_equal(expected, robot_1.is_off_the_boundary?)

    robot_2 = Robot.new(mars, 0, 0, 'N')
    robot_2.move_forward
    expected = false
    assert_equal(expected, robot_2.is_off_the_boundary?)
  end

  def test_leave_scent
    mars = Planet.new('Mars', 3, 3)
    robot_1 = Robot.new(mars, 1, 2, 'N')
    robot_1.leave_scent
    robot_mark = RobotMark.where(:planet => 'Mars', :x_coordinate => 1, :y_coordinate => 2).first
    expected_planet = 'Mars'
    expected_x_coordinate = 1
    expected_y_coordinate = 2
    assert_equal(false, robot_mark.nil?)
    assert_equal(expected_planet, robot_mark.planet)
    assert_equal(expected_x_coordinate, robot_mark.x_coordinate)
    assert_equal(expected_y_coordinate, robot_mark.y_coordinate)
  end

  def test_is_marked
    mars = Planet.new('Mars', 3, 3)
    robot_1 = Robot.new(mars, 1, 2, 'N')
    robot_1.leave_scent
    robot_2 = Robot.new(mars, 1, 2, 'N')
    robot_3 = Robot.new(mars, 0, 0, 'N')
    expected_robot_2_return = true
    expected_robot_3_return = false
    assert_equal(expected_robot_2_return, robot_2.is_marked?)
    assert_equal(expected_robot_3_return, robot_3.is_marked?)
  end
end
