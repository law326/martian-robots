require 'test_helper'

class UniverseTest < ActiveSupport::TestCase
  def test_create_universe
    instruction = "5  3\r\n1  1  E\r\nRFRFRFRF\r\n3  2  N\r\nFRRFLLFFRRFLL\r\n0  3  W\r\nLLFFFLFLFL\r\n"
    universe = Universe.new(instruction)
    expected_planet_name = 'Mars'
    expected_planet_x_boundary = 5
    expected_planet_y_boundary = 3
    expected_number_of_robots = 3
    assert_equal(expected_planet_name, universe.planet.name)
    assert_equal(expected_planet_x_boundary, universe.planet.x_boundary)
    assert_equal(expected_planet_y_boundary, universe.planet.y_boundary)
    assert_equal(expected_number_of_robots, universe.robots.count)

    expected_first_robot_initial_x_position = 1
    expected_first_robot_initial_y_position = 1
    expected_first_robot_initial_orientation = 'E'
    expected_first_robot_command = 'RFRFRFRF'
    assert_equal(expected_first_robot_initial_x_position, universe.robots[0][:robot].x_position)
    assert_equal(expected_first_robot_initial_y_position, universe.robots[0][:robot].y_position)
    assert_equal(expected_first_robot_initial_orientation, universe.robots[0][:robot].orientation)
    assert_equal(expected_first_robot_command, universe.robots[0][:command])

    expected_second_robot_initial_x_position = 3
    expected_second_robot_initial_y_position = 2
    expected_second_robot_initial_orientation = 'N'
    expected_second_robot_command = 'FRRFLLFFRRFLL'
    assert_equal(expected_second_robot_initial_x_position, universe.robots[1][:robot].x_position)
    assert_equal(expected_second_robot_initial_y_position, universe.robots[1][:robot].y_position)
    assert_equal(expected_second_robot_initial_orientation, universe.robots[1][:robot].orientation)
    assert_equal(expected_second_robot_command, universe.robots[1][:command])


    expected_third_robot_initial_x_position = 0
    expected_third_robot_initial_y_position = 3
    expected_third_robot_initial_orientation = 'W'
    expected_third_robot_command = 'LLFFFLFLFL'
    assert_equal(expected_third_robot_initial_x_position, universe.robots[2][:robot].x_position)
    assert_equal(expected_third_robot_initial_y_position, universe.robots[2][:robot].y_position)
    assert_equal(expected_third_robot_initial_orientation, universe.robots[2][:robot].orientation)
    assert_equal(expected_third_robot_command, universe.robots[2][:command])
  end
end
